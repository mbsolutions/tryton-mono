API Reference
*************

Product Deactivatable
=====================

.. class:: trytond.modules.product.TemplateDeactivatableMixin

   A :class:`~trytond:trytond.model.DeactivableMixin` that includes the soft
   deletion state of the ``template`` record for record soft deletion.

.. class:: trytond.modules.product.ProductDeactivatableMixin

   A :class:`~trytond.modules.product.TemplateDeactivatableMixin` that includes
   the soft deletion state of the ``product`` record for record soft deletion.
