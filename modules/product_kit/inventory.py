# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class InventoryLine(metaclass=PoolMeta):
    __name__ = 'stock.inventory.line'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.product.domain.append(('type', '!=', 'kit'))
