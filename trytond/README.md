mbs-7.0

[![pipeline status](https://gitlab.com/mbsolutions/tryton-mono/badges/mbs-7.0/pipeline.svg)](https://gitlab.com/mbsolutions/tryton-mono/-/commits/mbs-7.0)

[![coverage report](https://gitlab.com/mbsolutions/tryton-mono/badges/mbs-7.0/coverage.svg)](https://gitlab.com/mbsolutions/tryton-mono/-/commits/mbs-7.0)


trytond
=======

The server of the Tryton platform.

Installing
----------

See INSTALL

Note
----

This server may contain patches over the upstream Tryton server
needed for m9s modules to run.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/trytond/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, forum or IRC channel:

   * http://bugs.tryton.org/
   * http://www.tryton.org/forum
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
