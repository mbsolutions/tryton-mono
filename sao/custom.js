(function() {
    'use strict';
}());

Sao.config.limit = 1000;
Sao.config.display_size = 50;
Sao.config.roundup = {};
Sao.config.roundup.url = 'https://support.m9s.biz/';
Sao.config.title = 'MBSolutions';
Sao.config.icon_colors = '#43a299,#555753,#cc0000'.split(',');
Sao.config.bus_timeout = 10 * 60 * 1000;

/* Dashboard */

render_plot = function(chart_id, data, layout, config) {
    loadPlotly();
    Plotly.newPlot(chart_id, data, layout, config);
}

render_widget = function(json, el, chart_id) {
    el.empty()
    var chart = JSON.parse(json);
    var data = chart.data;
    var layout = chart.layout;
    var config = chart.config;
    if (data && data.length > 0) {
        type = data[0]['type'];
        if (type == 'value') {
            el.append(jQuery('<h1><center>' + data[0]['value'] + '</center></h1>'));
        } else if (type == 'error') {
            el.append(jQuery('<p>' + data[0]['message'] + '</p>'));
        } else {
            // https://plotly.com/javascript/reference/layout/
            layout.autosize = true;
            layout.separators = ',.';
            layout.margin = {
                t: 40,
                l: 40,
                r: 40,
                b: 40,
                autoexpand: false,
                width: 500
            };
            config.responsive = true;
            render_plot(chart_id, data, layout, config);
        }
    }
}

DashboardView = Sao.class_(Object, {
    init: function(attributes, context) {
        var Widget, view_prm;
        var attributes, attribute, node, actions_prms;

        this.attributes = attributes;
        this.context = context;
        this.widgets = {};
        this.el = jQuery('<div/>', {
            'class': 'content-box'
        });
    },
    setup: function() {
        var widgets;
        this.el.empty();
        widgets = JSON.parse(this.attributes['view']);
        this.create_widgets(widgets, this.el);
    },
    create_widgets: function(items, parent) {
        var item, widget_prm;
        Widget = new Sao.Model('babi.widget');

        table = jQuery('<table/>', {
            'style': 'width: 100%',
            'table-layout': 'fixed'
        }).appendTo(parent);
        var count = 0;
        var row;
        for (i = 0, len = items.length; i < len; i++) {
            var item = items[i];
            var widget_id = item['widget'];
            var colspan = item['colspan'];
            var height = item['height'];

            if ((count % 4) == 0) {
                row = jQuery('<tr/>').appendTo(table);
            }
            count += colspan;
            if (count >= 4) {
                count = 0;
            }

            widget_prm = Widget.execute('read', [
                [widget_id],
                ['chart']
            ], this.context)
            w = jQuery('<td/>', {
                // TODO: It will not work if the same widget is shown twice
                'id': 'chart-' + widget_id,
                'width': 70,
                'word-wrap': 'break-word',
                'colspan': colspan,
                'style': 'height: ' + height + 'px; vertical-align: top;'
            }).appendTo(row);
            this.widgets[widget_id] = w;
            widget_prm.done(response => {
                var record;
                record = response[0];
                render_widget(record['chart'], this.widgets[record['id']],
                    'chart-' + record['id']);
            })
        }
    },
    reload: function() {
        this.setup();
    },
});

DashboardTab = Sao.class_(Sao.Tab, {
    class_: 'tab-board',
    init: function(attributes) {
        var Dashboard, view_prm;
        Sao.Tab.Board._super.init.call(this, attributes);
        this.dashboard_id = attributes.dashboard;
        this.context = attributes.context;
        this.name = attributes.name;
        this.dialogs = [];
        this.board = null;
        Dashboard = new Sao.Model('babi.dashboard');
        this.view_prm = Dashboard.execute('read', [
            [this.dashboard_id],
            ['name', 'view']
        ], this.context);
        this.view_prm.done(response => {
            var board, record;
            record = response[0];
            this.set_name(record.name);
            this.board = new DashboardView(record, this.context);
            this.board.setup();
            this.content.append(this.board.el);
        });
        this.create_tabcontent();
        this.set_name(this.name);
        this.title.text(this.name_el.text());
    },
    compare: function(attributes) {
        if (this.attributes.dashboard != attributes.dashboard) {
            return false;
        }
        if (this.attributes.context != attributes.context) {
            return false
        }
        return true;
    },
    reload: function() {
        this.board.setup();
    },
    record_message: function() {
    },
    refresh_resources: function() {
    },
    update_resources: function() {
    },
});

ChartFormWidget = Sao.class_(Sao.View.Form.Widget, {
    class_: 'form-chart',
    expand: true,
    init: function(view, attributes) {
        ChartFormWidget._super.init.call(this, view, attributes);
        this.chart_id = 'abcd';
        this.el = jQuery('<div/>', {
            'id': this.chart_id,
            'class': this.class_
        });
    },
    display: function() {
        ChartFormWidget._super.display.call(this);
        if (!this.record) {
            return;
        }
        this.el.empty();
        var value = this.record.field_get_client(this.field_name);
        if (!value) {
            return;
        }
        render_widget(value, this.el, this.chart_id);
    },
    focus: function() {
    },
    get modified() {
        return false;
    },
    get_value: function() {
        return null;
    },
    set_value: function() {
    },
    set_readonly: function(readonly) {
        ChartFormWidget._super.set_readonly.call(this, readonly);
    }
});

/* Register chart factory */


Sao.View.FormXMLViewParser.WIDGETS['chart'] = ChartFormWidget;

/* Override core existing Sao.Action.exec_action */

Sao.Action.exec_action = function (core) {
    return function (action, data, context) {
        core(action, data, context);
        if (action.type == 'babi.action.dashboard') {
            // Check if tab already exists
            for (const other of Sao.Tab.tabs) {
                if (other.compare(action)) {
                    var tablist = jQuery('#tablist');
                    tablist.find('a[href="#' + other.id + '"]')
                        .tab('show')[0].scrollIntoView();
                    return;
                }
            }
            tab = new DashboardTab(action);
            Sao.Tab.add(tab);
        }
    };
}(Sao.Action.exec_action);


/* WIDGETS */

function loadStylesheetSync(url) {
    const css = jQuery('<link/>', {
        'rel': 'stylesheet',
        'href': url,
    })[0];

    // We override the 'onload' and 'onerror' function.
    css.onload = function() {};
    css.onerror = function(error) {
        console.error('Error while loading CSS in sync mode!', error);
    };

    document.head.appendChild(css);
}

function loadScriptSync(url) {
    const loader = new XMLHttpRequest();
    loader.open('GET', url, false); // <- 'false' is what makes the 'loader' sync

    loader.onreadystatechange = function() {
        if (loader.readyState === 4 && (loader.status === 200 || loader.status === 0)) {
            eval(loader.responseText);
        }
    };
    loader.send(null);

    const script = jQuery('<script/>', {
        'type': 'text/javascript',
        'text': loader.responseText
    })[0];
    document.head.appendChild(script);
}

function loadCSSAsync(url) {
    const headElem = document.head || document.getElementsByTagName('head')[0];
    const css = jQuery('<style/>', {'type': 'text/css'})[0];

    if (css.styleSheet) {
        css.styleSheet.cssText = url;
    } else {
        css.appendChild(document.createTextNode(url));
    }

    headElem.appendChild(css);
}

function loadPlotly() {
    if (typeof Plotly == 'undefined') {
        loadScriptSync('https://cdn.plot.ly/plotly-2.27.1.min.js');
    }
}

function loadMonaco() {
    // Plotly must be loaded before Monaco due to the issue described here:
    // https://stackoverflow.com/questions/55057425/can-only-have-one-anonymous-define-call-per-script-file
    // So whenever we need monaco we must ensure that plotly is loaded first.
    // In some cases we will not need plotly but if we need it afterwards and
    // trying to load it will fail.
    loadPlotly();
    loadEditorJS();
    if (typeof monaco == 'undefined') {
        loadStylesheetSync('https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.44.0/min/vs/editor/editor.main.min.css');
        loadScriptSync('https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.44.0/min/vs/loader.min.js');
    }
}

function loadEditorJS() {
    if (typeof EditorJS == 'undefined') {
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/editorjs@latest');

        loadScriptSync('/paragraph_editor/paragraph.js');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/header@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/quote@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/delimiter@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/editorjs-toggle-block');

        loadScriptSync('/list_editor/list.js');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/checklist@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/nested-list@latest');

        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/embed@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/image@latest');

        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/table@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/link@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/raw@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/footnotes@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/warning@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/marker@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/attaches@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/inline-code@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/code@latest');

        loadScriptSync('https://cdn.jsdelivr.net/npm/editorjs-drag-drop');

        loadScriptSync('https://cdn.jsdelivr.net/npm/@editorjs/text-variant-tune@latest');
        loadScriptSync('https://cdn.jsdelivr.net/npm/editorjs-text-alignment-blocktune@latest');
    }
}

const Focus = {
    In: 'In',
    OutNoSave: 'OutNoSave',
    OutSave: 'OutSave',
};

let autoIncrement = 0;

/* EDITOR.JS (block editor) */

const Database =
{
    Delete: 'Delete',
    Add: 'Add',
}

BlockFormWidget = Sao.class_(Sao.View.Form.Widget, {
    class_: 'form-block',
    expand: true,

    init: function(view, attributes) {
        BlockFormWidget._super.init.call(this, view, attributes);

        this.editor_id = "editor-js-" + (autoIncrement++);

        // Root container
        this.el = jQuery('<div/>', {
            'class': this.class_,
            'id': this.editor_id,
        });

        // To ensure that we can add the 'focusin' event
        const body = document.querySelector('body');
        this.focus = Focus.OutNoSave;
        if (body && this.el[0]) {
            body.addEventListener('focusin', (event) => {
                // If the focus is not in our widget, but before it was, put 'OutSave'
                if (!this.el[0].contains(event.target)) {
                    if (this.focus == Focus.In) {
                        this.focus = Focus.OutSave;
                    }
                } else {
                    if (this.focus != Focus.In) {
                        this.focus = Focus.In;
                    }
                }

                // When the 'focus' is defined like 'OutSave', call to 'focus_out' for enable the 'save' button
                if (this.focus == Focus.OutSave) {
                    this.focus_out();
                    this.focus = Focus.OutNoSave;
                }
            });
        };

        // A variable to avoid useless 'display' calls after we save the record.
        this._saving = false;
        // A necessary variable to avoid some problems when change between records with display.
        this._lastRecordID = undefined;

        // For override/bind the 'this' inside the 'uploadByFile' function
        inClassUploadByFile = this.uploadByFile.bind(this);

        loadEditorJS();
        this._editor = new EditorJS({
            holder: this.el[0],
            tools: {
                paragraph: Paragraph,
                header: Header,
                list: {
                    class: List,
                    config: {
                        defaultStyle: 'unordered'
                    }
                },
                embed: Embed,
                image: {
                    class: ImageTool,
                    config: {
                        uploader: {
                            uploadByFile(file) {
                                return inClassUploadByFile(file);
                            }
                        }
                    }
                },
                checklist: Checklist,
                table: Table,
                raw: RawTool,
                delimiter: Delimiter,
                inlineCode: {
                    class: InlineCode,
                    shortcut: 'CMD+M',
                },
                warning: Warning,
                code: CodeTool,
                Marker: {
                    class: Marker,
                },
                quote: {
                    class: Quote,
                    inlineToolbar: true,
                    config: {
                        quotePlaceholder: 'Quote...',
                        captionPlaceholder: 'Author...',
                    },
                },
                attaches: {
                    class: AttachesTool,
                    config: {
                        uploader: {
                            uploadByFile(file) {
                                return inClassUploadByFile(file);
                            }
                        }
                    }
                },
                footnotes: {
                    class: FootnotesTune,
                },
                toggle: {
                    class: ToggleBlock,
                    inlineToolbar: true
                },

                // Tunes
                textVariant: TextVariantTune,
                textAlign: AlignmentBlockTune,
            },

            // Define of the tunes
            tunes: ['textVariant', 'textAlign'],

            onChange: (api, event) => {
                this.updateData();
            },

            onReady: () => {
                new DragDrop(this._editor);

                // We put a left padding for avoid z-index problems with the 'Add' and 'Tune' menu.
                // Also, we delete the unecessary padding of the redactor editor.
                const elements = document.querySelectorAll('.codex-editor__redactor');
                elements.forEach(elem => {
                    elem.style.paddingBottom = 0;
                });
            }
        });
    },

    uploadByFile: function(file) {
        if (this.record.id < 0) {
            return;
        }
        return file.arrayBuffer().then(data => {
            const view = new Uint8Array(data);

            return Sao.rpc({
                'method': 'model.ir.attachment.create',
                'params': [
                    [{
                        'name': file.name,
                        'data': view,
                        'resource': [this.record.model.name, this.record.id],
                        //'type': file.type,
                        //'link': '',
                    }],
                    Sao.Session.current_session.context
                ]
            }, Sao.Session.current_session).then((id) => {
                return {
                    success: 1,
                    file: {
                        'url': Sao.Session.current_session.database + '/widgets/attachment/'+ String(id[0])
                    }
                }
            });
        })
    },
    editor: function() {
        return this._editor
    },
    updateData: async function() {
        // To notify the changes to Tryton
        this.send_modified();
        this.editor().save().then((output) => {
            // Transform the data into 'string' type and store it in a variable
            this.value = JSON.stringify(output);
        })
        .catch((error) => console.error(error));
    },
    toggleDatabase: function(value, type) {
        if (!value) {
            return;
        }

        // Parse the URL data
        let valueTemp = JSON.parse(value);
        // We check if there is some block that have data (NOTE: Only support Attachments and Images)
        valueTemp.blocks.forEach(item => {
            const file = item.data.file;
            if (!file || !file.url) return;

            const parts = file.url.split("/");

            if (type === Database.Delete) {
                // Check if the change is necessary
                if (URL.canParse(file.url)) return;
                else if (!file.url.includes("widgets/attachment")) return;
                else if (parts[0] === "widgets") return;

                // Delete the first part (the DATABASE)
                parts.shift();
            } else if (type === Database.Add) {
                // Check if the change is necessary
                if (URL.canParse(file.url)) return;
                else if (!file.url.includes("widgets/attachment")) return;
                else if (parts[0] === Sao.Session.current_session.database) return;

                // Add the DATABASE
                parts.unshift(Sao.Session.current_session.database);
            }

            // Create the new URL without the DATABASE
            let urlString = "";
            for (let iPos = 0 ; iPos < parts.length ; iPos++) {
                urlString += parts[iPos] + "/";
            }

            urlString = urlString.substring(0, urlString.length - 1);

            item.data.file.url = urlString;
        });

        valueTemp = JSON.stringify(valueTemp);
        return valueTemp;
    },
    display: function() {
        BlockFormWidget._super.display.call(this);

        // If there is no 'record' created, exit
        if (!this.record) {
            return;
        } else if (this._lastRecordID === this.record.id) {
            if (this._saving) {
                return;
            }
        }

        this._lastRecordID = this.record.id;

        // If the actual 'record' exists in the DB, get the value stored in the DB
        this.value = this.record.field_get_client(this.field_name);

        // Add the database name to the URL images and attaches.
        // This is being made for make EditorJS able to load the data.
        this.value = this.toggleDatabase(this.value, Database.Add);

        // If the value not exists, exit
        if (!this.value) {
            this._editor.clear();
            return;
        }

        // Parse the value for loading the data into the EditorJS instance
        const parsedValue = JSON.parse(this.value);
        this._editor.isReady.then(() => {
            this._editor.render(parsedValue);
        });

        // We delete the database name of all URLs images and attaches.
        // This is being made, firstly, for avoid a problem with 'get modified()',
        // which get the data of the BD, and that data don't have the database name in the URLs.
        this.value = this.toggleDatabase(this.value, Database.Delete);
    },
    focus: function() {},
    get modified() {
        if (this.record) {
            return this.record.field_get_client(this.field_name) != this.value;
        }
        return false;
    },
    set_value: function() {
        let timeout;

        this._saving = true;
        clearTimeout(timeout);
        timeout = setTimeout(() => this._saving = false, 500);

        if (!this.value) {
            this.field.set_client(this.record, '{"blocks":[]}');
        }

        // Delete the database name to the URL images and attaches.
        this.value = this.toggleDatabase(this.value, Database.Delete);

        this.field.set_client(this.record, this.value);
    },
    set_readonly: function(readonly) {
        BlockFormWidget._super.set_readonly.call(this, readonly);
    }
});

Sao.View.FormXMLViewParser.WIDGETS['block'] = BlockFormWidget;


/* Code Editor (Monaco) */

CodeFormWidget = Sao.class_(Sao.View.Form.Widget, {
    class_: 'code-block',
    expand: true,

    init: function(view, attributes) {
        CodeFormWidget._super.init.call(this, view, attributes);

        this.code_id = "code-" + (autoIncrement++);

        // The root tag of the widget
        this.el = jQuery('<div/>', {
            'class': this.class_,
            'id': this.code_id,
        });

        // To ensure that we can add the 'focusin' event
        const body = document.querySelector('body');
        this.focus = Focus.OutNoSave;
        if (body && this.el[0]) {
            body.addEventListener('focusin', (event) => {
                if (!this.el[0].contains(event.target)) {
                    if (this.focus == Focus.In) {
                        this.focus = Focus.OutSave;
                    }
                } else {
                    this.focus = Focus.In;
                }

                if (this.focus == Focus.OutSave) {
                    this.focus_out();
                    this.focus = Focus.OutNoSave;
                }
            });
        };

        this.code = null;

        loadMonaco();

        // We load the monaco link for proper creation instance
        require.config({
            paths: {
                'vs': 'https://cdnjs.cloudflare.com/ajax/libs/monaco-editor/0.44.0/min/vs'
            }
        });

        // We create the MONACO instance
        require(["vs/editor/editor.main"], () => {
            this.code = monaco.editor.create(this.el[0], {
                value: '',
                language: 'html',
                theme: 'vs-dark',
                automaticLayout: true,
            });
            send_modified = this.send_modified.bind(this);
            this.code.getModel().onDidChangeContent(function(event) {
                send_modified();
            });
            this.display();
        });
    },

    display: function() {
        CodeFormWidget._super.display.call(this);
        if (!this.code) {
            return;
        }
        let value = null;
        if (this.record) {
            value = this.record.field_get_client(this.field_name);
        }
        if (!value) {
            value = '';
        }
        this.code.setValue(value);
    },

    focus: function() {},

    get modified() {
        if (!this.code) {
            return false;
        }

        if (this.record) {
            return this.record.field_get_client(this.field_name) != this.code.getValue();
        }
        return false;
    },

    set_value: function() {
        if (!this.code) {
            return;
        }
        this.field.set_client(this.record, this.code.getValue());
    },

    set_readonly: function(readonly) {
        CodeFormWidget._super.set_readonly.call(this, readonly);
        if (!this.code) {
            return;
        }
        this.code.updateOptions({
            readOnly: readonly
        });
    }
});

Sao.View.FormXMLViewParser.WIDGETS['code'] = CodeFormWidget;
